import  serverService from '../server.service';
import fetchMock from 'jest-fetch-mock';

describe('findServer', () => {
    beforeEach(() => {
        fetchMock.enableMocks();
    });
    afterEach(() => {
        fetchMock.resetMocks();
    });
    it('should return the server with the lowest priority', async () => {
        const servers = [
            {
                "url": "https://does-not-work.perfume.new",
                "priority": 1
            },
            {
                "url": "https://gitlabww.com",
                "priority": 4
            },
            {
                "url": "http://app.scnt.me",
                "priority": 3
            },
            {
                "url": "https://offline.scentronix.com",
                "priority": 2
            }
        ];
        fetchMock.mockResponseOnce(JSON.stringify({ message: "Server is online" }), { status: 200 });
        const result = await serverService.findLowPriorityServer(servers);
        expect(result.lowestPriorityServer).toEqual("http://app.scnt.me");
    })
})