import { IconButton, MenuItem, Typography } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu'
import Menu from '@mui/material/Menu';
import React from 'react'

type Hambar = {
    handleOpenNavMenu: (event: React.MouseEvent<HTMLElement>) => void;
    anchorElNav: HTMLElement | null;
    handleCloseNavMenu: () => void;
    pages: string[];
}

const Hambar = (props: Hambar) => {
    const {handleOpenNavMenu, anchorElNav, handleCloseNavMenu, pages} = props;
    return (
        <>
        <IconButton
            size="large"
            aria-label="page-links"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            onClick={handleOpenNavMenu}
        >
            <MenuIcon />
        </IconButton>
        <Menu
            id="menu-appbar"
            anchorEl={anchorElNav}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
            }}
            keepMounted
            transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
            }}
            open={Boolean(anchorElNav)}
            onClose={handleCloseNavMenu}
            sx={{
                display: { xs: 'block', md: 'none' },
            }}
        >
            {pages.map((page) => (
                <MenuItem key={page} onClick={handleCloseNavMenu}>
                    <Typography textAlign="center" sx={{color: 'black'}}>{page}</Typography>
                </MenuItem>
            ))}
        </Menu>
        </>
    )
}

export default Hambar;
