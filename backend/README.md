# Backend App

Its a back-end server of test. follow the instructions to run the server

# Prerequisites

    - node v16+

# Build and run

    build project
        `npm run build`
    run built project as local
        `npm start`

