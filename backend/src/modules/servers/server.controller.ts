import { Router, Request, Response } from 'express';
import { serversPriorityList } from '../../constants/search';
import ServerService from './server.service';

export class ServerController {
  public router: Router;
  public serverService = ServerService;

  constructor() {
    this.router = Router();
    this.routes();
  }

  public findServer = async (req: Request, res: Response) => {
    try {
      const getLowPriorityServer = await this.serverService
        .findLowPriorityServer(serversPriorityList);
      res.send(getLowPriorityServer);
    } catch (error: any) {
      res.status(500).json({
        error: true,
        message: error.message,
      });
    }
  }

  public routes() {
    this.router.get('/findServer', this.findServer);
    return this.router
  }
}
