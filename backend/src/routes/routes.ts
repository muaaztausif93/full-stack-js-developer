import { Request, Response } from 'express';
import { ServerController } from '../modules/servers/server.controller';

const serverController = new ServerController();
export const noAuthRoutes = [
  {
    path: '/server',
    middleware: [],
    action: serverController.routes(),
  },
];

export const AppRoutes = [
  {
    path: '/demo',
    middleware: [],
    action: () => {},
  },
];
