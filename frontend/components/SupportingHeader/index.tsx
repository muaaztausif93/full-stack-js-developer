import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Container from '@mui/material/Container';
import HeaderLinkComponent from '../HeaderLink';
import { styled } from '@mui/material';
import { withHeader } from '../WithHeader';

type HeaderType = {
  supportingPages: string[];
  handleLinkClick: (page: string) => void;
  activePage: string;
}

const RootContainer = styled('div')(({ theme }) => ({
    paddingLeft: '230px',
    width: '100%',
    [theme.breakpoints.down('md')]: {
        paddingLeft: '30px',
        paddingRight: '30px',
    },
    [theme.breakpoints.up('xl')]: {
        paddingLeft: '0px',
        paddingRight: '0px',
    },
  }));

const AdjustedBox = styled('div')(({ theme }) => ({
    flexGrow: 1,
    display: 'flex',
    [theme.breakpoints.down('xs')]: {
        flexGrow: 1,
        display: 'flex',
        justifyContent:'center'
    },
    [theme.breakpoints.down('md')]: {
        flexGrow: 1,
        display: 'flex',
        justifyContent:'center'
    },
    [theme.breakpoints.up('xl')]: {
        flexGrow: 1,
        display: 'flex',
        justifyContent:'center'
    },
}))

function SupportingHeader(props: HeaderType) {
  const { supportingPages } = props;
  const [activeLinkPage, setActiveLinkPage] = React.useState('')
  const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(null);

  const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElNav(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  return (
    <AppBar position="static" sx={{backgroundColor: '#F8F5EF', boxShadow: 'none'}}>
        <RootContainer>
                <Toolbar disableGutters>
                <AdjustedBox>
                    {supportingPages.map((page, index) => (
                    <div key={index}>
                        <HeaderLinkComponent
                          page={page}
                          index={index}
                          activeLinkPage={props.activePage}
                          setActiveLinkPage={props.handleLinkClick}
                          paddingX={"10px"}
                          fontSize={"12px"}
                        />
                    </div>
                    ))}
                </AdjustedBox>
                </Toolbar>
        </RootContainer>
    </AppBar>
  );
}
export default withHeader(SupportingHeader);