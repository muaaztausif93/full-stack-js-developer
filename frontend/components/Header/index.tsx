import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Hambar from '../Hambar';
import HeaderLinkComponent from '../HeaderLink';
import { withHeader } from '../WithHeader';

type HeaderType = {
  pages: string[];
  activePage: string;
  handleLinkClick: (page: string) => void;
}

function ResponsiveAppBar(props: HeaderType) {
  const { pages } = props;
  const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(null);

  const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElNav(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  return (
    <AppBar position="static" sx={{backgroundColor: 'white', boxShadow: 'none', height: '10vh', justifyContent: 'center'}}>
      <Container maxWidth="xl">
        <Toolbar disableGutters sx={{paddingX: 10}}>
          <Typography
            variant="h6"
            noWrap
            component="a"
            href="/"
            sx={{
              mr: 2,
              display: { xs: 'none', md: 'flex' },
              fontFamily: 'Roboto, sans-serif',
              fontWeight: 700,
              letterSpacing: '.3rem',
              color: '#444444',
              textDecoration: 'none',
            }}
          >
            BAKERY
          </Typography>
          <Typography
            variant="h5"
            noWrap
            component="a"
            href="/"
            sx={{
              mr: 2,
              display: { xs: 'flex', md: 'none' },
              flexGrow: 1,
              fontWeight: 700,
              letterSpacing: '.3rem',
              color: '#444444',
              textDecoration: 'none',
            }}
          >
            BAKERY
          </Typography>
          <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
            {pages.map((page, index) => (
              <div key={index}>
                <HeaderLinkComponent
                  page={page}
                  index={index}
                  activeLinkPage={props.activePage}
                  setActiveLinkPage={props.handleLinkClick}
                />
              </div>
            ))}
          </Box>
          <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none', justifyContent: 'flex-end' } }}>
            <Hambar
              handleOpenNavMenu={handleOpenNavMenu}
              anchorElNav={anchorElNav}
              handleCloseNavMenu={handleCloseNavMenu}
              pages={pages}
            />
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
}
export default withHeader(ResponsiveAppBar);