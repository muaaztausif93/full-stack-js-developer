/** @format */

import * as React from 'react';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
import Stack from '@mui/material/Stack';
import { Box } from '@mui/material';

function handleClick(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) {
  event.preventDefault();
}

export default function BreadCrumbs() {
  const breadcrumbs = [
    <Link
      underline="hover"
      key="1"
      color="inherit"
      href="/"
      onClick={handleClick}
      sx={{
        fontFamily: 'Roboto, Helvetica, Arial, sans-serif',
        fontWeight: 600,
        fontSize: '0.875rem',
        lineHeight: 1.75,
        letterSpacing: '0.02857em',
        textTransform: 'uppercase',
        color: '#525151',
      }}
    >
      RECIPES
    </Link>,
    <Link
      underline="hover"
      key="2"
      color="inherit"
      href="/material-ui/getting-started/installation/"
      onClick={handleClick}
      sx={{
        fontFamily: 'Roboto, Helvetica, Arial, sans-serif',
        fontWeight: 600,
        fontSize: '0.875rem',
        lineHeight: 1.75,
        letterSpacing: '0.02857em',
        textTransform: 'uppercase',
        color: '#525151',
      }}
    >
      BREAD
    </Link>,
    <Typography
      key="3"
      sx={{
        fontFamily: 'Roboto, Helvetica, Arial, sans-serif',
        fontWeight: 600,
        fontSize: '0.875rem',
        lineHeight: 1.75,
        letterSpacing: '0.02857em',
        textTransform: 'uppercase',
        color: '#525151',
      }}
    >
      QUICK BREAD
    </Typography>,
    <Box  key="4"></Box>,
  ];

  return (
    <Stack spacing={1}>
      <Breadcrumbs
        separator="›"
        aria-label="breadcrumb"
        sx={{
          '& .MuiBreadcrumbs-separator': {
            color: 'red',
          },
        }}
      >
        {breadcrumbs}
      </Breadcrumbs>
    </Stack>
  );
}
