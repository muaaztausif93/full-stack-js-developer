import { ServerPriorityList } from "./server.types";
import { Network } from "../../utils/network";

class ServerService {
    private network;
    constructor() {
        this.network = new Network();
    }
    public async findLowPriorityServer(serversPriorityList: ServerPriorityList) {
        let lowestPriorityServer;
        let lowestPriority = Number.MAX_SAFE_INTEGER;
        const serverPriorityListPromises = serversPriorityList.map(server => {
            return this.network.get(server.url, '' ,{ timeout: 5000 })
                .then(serverResponse => {
                    if (serverResponse.status >= 200 && serverResponse.status < 300) {
                        return {
                            status: 'fulfilled',
                            priority: server.priority,
                            url: server.url
                        };
                    } else {
                        return {
                            status: 'rejected',
                            priority: Number.MAX_SAFE_INTEGER,
                            url: server.url
                        };
                    }
                })
                .catch(error => {
                    return {
                        status: 'rejected',
                        priority: Number.MAX_SAFE_INTEGER,
                        url: server.url
                    };
                });
        });
        const serverResponses = await Promise.allSettled(serverPriorityListPromises)
        serverResponses.forEach(serverResponse => {
            if (serverResponse.status === 'fulfilled' && serverResponse.value.status === 'fulfilled') {
                if (serverResponse.value.priority < lowestPriority) {
                    lowestPriority = serverResponse.value.priority;
                    lowestPriorityServer = serverResponse.value.url;
                }
            }
        });
        if (!!lowestPriorityServer) {
            return {
                lowestPriorityServer,
                priority: lowestPriority
            };
        } else {
            throw new Error("No online servers found");
        }
    }
}

export default new ServerService(); 