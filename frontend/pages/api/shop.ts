// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import { shopContentPageDescription, shopContentPageTitle } from '../../constants';

type Data = {
    contentPageTitle: string;
    contentPageDescription: string;
};

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const shopPage: Data = {
    contentPageTitle: shopContentPageTitle,
    contentPageDescription: shopContentPageDescription
  };
  res.status(200).send(shopPage);
}