import PageContent from '../components/PageContent';
import SupportingHeader from '../components/SupportingHeader';
import config from '../config';
import { supportingPages } from '../constants';

type HomeType = {
  supportingPages: string[];
  contentPageTitle: string;
  contentPageDescription: string;
}
export default function Shop(props: HomeType) {
  const {supportingPages, contentPageTitle, contentPageDescription} = props;
  return (
    <>
      <SupportingHeader supportingPages={supportingPages}/>
      <PageContent 
        contentPageTitle={contentPageTitle} 
        contentPageDescription={contentPageDescription}/>
    </>
  )
}

export async function getServerSideProps() {
  let res;
  let url = '';
  let contentPageData;
  config.isProduction ? 
    url = `https://${config.host}/api/shop` : 
    url = `http://${config.host}:${config.port}/api/shop`
  res = await fetch(url)
  contentPageData = await res.json();

  return {
    props: {
       contentPageTitle: contentPageData.contentPageTitle,
       contentPageDescription: contentPageData.contentPageDescription,
       supportingPages: supportingPages
    },
  }
}