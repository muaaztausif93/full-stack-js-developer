export type ServerPriorityList = {
    url: string,
    priority: number
}[]