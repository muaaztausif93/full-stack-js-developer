import React from 'react'
import { Grid } from '@mui/material';

type TextType = {
    title: string;
    description: string;
}
const TextComponent = (props: TextType) => {
    const { title, description } = props;
  return (
    <Grid container columns={16}>
        <Grid item display="flex"  justifyContent={'center'}>
          <Grid container columns={16} flexDirection='column' display="flex"  justifyContent={'center'}>
            <Grid 
              item 
              mb={1} 
              sx={{
                fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                lineHeight: 1,
                letterSpacing: '0.02857em',
                color: 'black',
                paddingRight: '5px',
                fontWeight: 600,
                fontSize: '0.8rem'
              }}
            >
              {title}
            </Grid>
            <Grid 
              item
              sx={{
                fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                lineHeight: 1,
                letterSpacing: '0.02857em',
                color: 'black',
                paddingRight: '5px',
                fontWeight: 500,
              }}
            >
              {description}
            </Grid>
          </Grid>
        </Grid>
    </Grid>
  )
}

export default TextComponent;
