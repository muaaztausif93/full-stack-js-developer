import axios from 'axios';

export class Network {

  public async get(baseUrl: string, path: string, options?: any) {
    const response = await axios.get(baseUrl + path, {
      ...options,
    });

    return response;
  }
}
