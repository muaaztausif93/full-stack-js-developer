import { Box } from '@mui/material'
import Link from 'next/link'
import React, { useEffect, useState } from 'react'

type HeaderLink = {
  page: string;
  index: number | string;
  activeLinkPage: string;
  setActiveLinkPage: (page: string) => void;
  paddingX?: string;
  fontSize?: string;
}
const HeaderLinkComponent = (props: HeaderLink) => {
  const {page, index, activeLinkPage, setActiveLinkPage, paddingX, fontSize} = props;
  const [activePage, setActivePage] = useState(activeLinkPage);

  useEffect(() => {
    setActivePage(activeLinkPage);
  }, [activeLinkPage])

  return (
    <Box
      key={index}
      onClick={() => setActiveLinkPage(page)}
      sx={{ 
        my: 2, 
        color: '#444444', 
        display: 'block', 
        paddingX: paddingX ?? '20px',
        fontFamily: "Roboto, Helvetica, Arial, sans-serif",
        fontWeight: 500,
        fontSize: '0.875rem',
        lineHeight: 1.75,
        letterSpacing: '0.02857em',
        textTransform: 'uppercase'
      }}
    >
      <Link 
        href={`/${page.toLocaleLowerCase()}`} 
        key={index} 
        style={{
          borderBottom: activePage === page ? '2px solid #BA474D' : 'none', 
          paddingBottom: '5px',
          transition: 'border-bottom 0.2s ease-out',
          fontSize: fontSize ?? '13px'
        }}>
        {page}
      </Link>
    </Box>
  )
}

export default HeaderLinkComponent
