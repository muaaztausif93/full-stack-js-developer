import { Box, Grid, ImageList, ImageListItem, styled, Typography } from '@mui/material';
import React from 'react'
import Image from 'next/image'
import AddIcon from '@mui/icons-material/Add';
import LocalPrintshopOutlinedIcon from '@mui/icons-material/LocalPrintshopOutlined';
import BreadCrumbs from '../Breadcrumbs';
import IconTextComponent from '../IconTextComponent';
import TextComponent from '../TextComponent';

const Title = styled('h3')(({ theme }) => ({
    fontFamily: "Roboto, Helvetica, Arial, sans-serif",
    fontWeight: 450,
    fontSize: '3rem',
    lineHeight: 1,
    letterSpacing: '0.02857em',
    color: 'black',
    marginTop: '30px',
    marginBottom: '110px',
  }));

type PageContent = {
    contentPageTitle: string;
    contentPageDescription: string;
}
const PageContent = (props: PageContent) => {
  return (
    <Grid container columns={16} sx={{padding: '35px 100px'}}>
        <Grid item xs={16} sm={16} md={8} mb={5} sx={{paddingRight: '30px'}}>
            <Box>
                <BreadCrumbs/>
                    <Title>
                        {props.contentPageTitle}
                    </Title>
                <Typography sx={{color: '#4e4e4e', marginBottom: "10px"}}>
                    {props.contentPageDescription} 
                </Typography>
            </Box>
            <Grid container columns={16} display="flex" justifyContent={'center'}>
                <Grid item xs={5} sm={5} md={5}>
                    <IconTextComponent
                        IconType={"Time"}
                        title={'PREP'}
                        description={"10 mins"}
                    />
                </Grid>
                <Grid item xs={6} sm={6} md={6} display="flex" justifyContent={'center'}>
                    <TextComponent
                        title={'BAKE'}
                        description={"1hr to 1hr 15 mins"}
                    />
                </Grid>
                <Grid item xs={5} sm={5} md={5} display="flex" justifyContent={'center'}>
                    <TextComponent
                        title={'TOTAL'}
                        description={"1hr 10 mins"}
                    />
                </Grid>
            </Grid>
            <hr style={{ width: '100%', marginTop: '30px', marginBottom: '30px', backgroundColor: '#333' }}/>
            <Grid container columns={16} display="flex" justifyContent={'center'}>
                <Grid item xs={5} sm={5} md={5}>
                    <IconTextComponent
                        IconType={"Yield"}
                        title={'YIELD'}
                        description={"1 loaf, 12 generous serving"}
                    />
                </Grid>
                <Grid item xs={6} sm={6} md={6} pr={1} display="flex" justifyContent={'center'}>
                    <Box 
                        sx={{
                            color:'black', 
                            border: '2px solid #de4d52',
                            fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                            letterSpacing: '0.02857em',
                            display: 'flex', 
                            justifyContent: 'center', 
                            alignItems: 'center',
                            width: '100%',
                            height: 'fit-content',
                            paddingTop: '2px',
                            paddingBottom: '2px',
                            cursor: 'pointer'
                        }}
                    >
                         <Box style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                            <AddIcon style={{paddingRight: '2px'}}/>
                            <Box sx={{
                                fontFamily: 'Roboto, sans-serif',
                                fontWeight: 700,
                                textDecoration: 'none',
                                fontSize: '0.7rem'
                            }}>
                                SAVE RECIPE
                            </Box>
                         </Box>
                    </Box>
                </Grid>
                <Grid item xs={5} sm={5} md={5} display="flex" justifyContent={'center'}>
                    <Box 
                        sx={{
                            color:'black', 
                            border: '2px solid #de4d52',
                            fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                            letterSpacing: '0.02857em',
                            display: 'flex', 
                            justifyContent: 'center', 
                            alignItems: 'center',
                            width: '100%',
                            height: 'fit-content',
                            paddingTop: '2px',
                            paddingBottom: '2px',
                            cursor: 'pointer'
                        }}
                    >
                        <Box style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                            <LocalPrintshopOutlinedIcon style={{paddingRight: '2px'}}/>
                            <Box sx={{
                                fontFamily: 'Roboto, sans-serif',
                                fontWeight: 700,
                                textDecoration: 'none',
                                fontSize: '0.7rem'
                            }}>
                                PRINT
                            </Box>
                        </Box>
                    </Box>
                </Grid>
            </Grid>
        </Grid>
        <Grid item xs={16} sm={16} md={8} display="flex" justifyContent={'center'}>
            <ImageList sx={{ width: 600, height: 400 }}>
                <ImageListItem key={"/bread.png"}>
                    <Image
                        src="/bread.png"
                        alt="Bread Pic"
                        width={600}
                        height={400}
                    />
                </ImageListItem>
            </ImageList>
        </Grid>
    </Grid>
  )
}

export default PageContent
