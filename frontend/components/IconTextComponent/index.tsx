import React from 'react'
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import DataSaverOffOutlinedIcon from '@mui/icons-material/DataSaverOffOutlined';
import { Grid } from '@mui/material';

type IconTextType = {
  IconType: string;
  title: string;
  description: string;
}

const IconTextComponent = (props: IconTextType) => {
  const { IconType,  title, description} = props;
  return (
    <Grid container columns={16}>
        <Grid 
          item  
          display="flex" 
          justifyContent={'center'} 
          alignItems="center"
          xs={4}
          sm={4}
          md={4}
          pr={2}
        >
          {IconType === 'Time' ? 
            <AccessTimeIcon 
              style={{
                fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                fontSize: '3rem',
                lineHeight: 1,
                letterSpacing: '0.02857em',
                color: 'black',
                paddingRight: '5px',
                fontWeight: 200,
              }}
            /> : 
            <DataSaverOffOutlinedIcon
              style={{
                fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                fontSize: '3rem',
                lineHeight: 1,
                letterSpacing: '0.02857em',
                color: 'black',
                paddingRight: '5px',
                fontWeight: 200,
              }}
            />
          }
        </Grid>
        <Grid item display="flex"  justifyContent={'center'} xs={12} sm={12} md={12}>
          <Grid container columns={16} flexDirection='column' display="flex"  justifyContent={'center'}>
            <Grid 
              item 
              mb={1} 
              sx={{
                fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                lineHeight: 1,
                letterSpacing: '0.02857em',
                color: 'black',
                paddingRight: '5px',
                fontWeight: 600,
                fontSize: '0.8rem'
              }}
            >
              <span>
                {title}
              </span>
            </Grid>
            <Grid 
              item
              sx={{
                fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                lineHeight: 1,
                letterSpacing: '0.02857em',
                color: 'black',
                paddingRight: '5px',
                fontWeight: 500,
              }}
            >
              <span>
                {description}
              </span>
            </Grid>
          </Grid>
        </Grid>
    </Grid>
  )
}

export default IconTextComponent;
