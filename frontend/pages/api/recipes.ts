// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import { recipeContentPageDescription, recipeContentPageTitle } from '../../constants';

type Data = {
    contentPageTitle: string;
    contentPageDescription: string;
};

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const recipePage: Data = {
    contentPageTitle: recipeContentPageTitle,
    contentPageDescription: recipeContentPageDescription
  };
  res.status(200).send(recipePage);
}