import convict from 'convict';

export const APP_CONFIG = convict({
  env: {
    doc: 'The application environment.',
    format: ['dev', 'production', 'test', 'staging', 'local'],
    default: 'dev',
    env: 'NODE_ENV',
  },
  ports: {
    PORT: {
      doc: 'The server port to bind.',
      format: 'port',
      default: 8000,
      env: 'PORT',
    },
  },
  secrets: {
    SESSION_SECRET: {
      doc: 'session secret',
      format: String,
      default: 'JQWERTY',
      env: 'SESSION_SECRET',
    },
  },
}).validate({ allowed: 'strict' })