export default  {
    isProduction: process.env.NODE_ENV === 'production' ? true : false,
    host: process.env.host || 'localhost',
    port: process.env.port || 3000,
}