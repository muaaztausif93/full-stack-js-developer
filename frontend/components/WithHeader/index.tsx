import React, { useEffect, useState } from 'react';

export const withHeader = (WrappedComponent: any ) => {
  const ComponentWithHeader = (props: any) => {
    const [activePage, setActivePage] = useState("/");
    const handleLinkClick = (page: string) => {
      setActivePage(page);
    }

    useEffect(() => {
      setActivePage(window.location.pathname.split('/')[1].toUpperCase());
    }, []);

    return (
      <div>
        <WrappedComponent
          activePage={activePage}
          handleLinkClick={handleLinkClick}
          {...props}
        />
      </div>
    );
  }
  return ComponentWithHeader;
};
